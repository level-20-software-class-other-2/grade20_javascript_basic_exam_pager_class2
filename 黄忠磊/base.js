// 题目如下，请直接利用写好的方法，并且直接写在题目下方，要可以运行和输出，如第1题所示

/*
1、题目描述：
找出元素 item 在给定数组 arr 中的位置
输出描述:
如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
示例1
输入
[ 1, 2, 3, 4 ], 3
输出
2

function indexOf(arr, item) {

}
*/
console.log(`======= 第1题 =======`);
function indexOf(arr, item) {
    return console.log(arr.indexOf(item));
}
indexOf([1, 2, 3, 4], 3)
/*
2、题目描述：
计算给定数组 arr 中所有元素的总和
输入描述:
数组中的元素均为 Number 类型
示例1
输入

[ 1, 2, 3, 4 ]
输出

10

function sum(arr) {

}
*/
console.log(`======= 第2题 =======`);

function sum(arr) {

    return arr.reduce(function (x, y) {
        return x + y
    })
}
console.log(sum([1, 2, 3, 4]));

/*
3、题目描述
在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4],  10
输出

[1, 2, 3, 4, 10]

function append(arr, item) {
    
}
*/
console.log(`======= 第3题 =======`);

function append(arr, item) {
    var arrs = arr.slice()
    arrs.push(item)
    console.log(arrs);
}

append([1, 2, 3, 4], 10)
/*
4、题目描述
合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], ['a', 'b', 'c', 1]
输出

[1, 2, 3, 4, 'a', 'b', 'c', 1]

function concat(arr1, arr2) {

}
*/
console.log(`======= 第4题 =======`);

function concat(arr1, arr2) {
    var arrs = arr1.slice()

    console.log(arrs.concat(arr2));
}

concat([1, 2, 3, 4], ['a', 'b', 'c', 1])
/*
5、题目描述
为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[1, 4, 9, 16]

function square(arr) {

}
*/
console.log(`======= 第5题 =======`);

function square(arr) {
    return arr.map((x) => x * x)
}
console.log(square([1, 2, 3, 4]));
/*
6、题目描述
定义一个计算圆面积的函数area_of_circle()，它有两个参数：

r: 表示圆的半径；
pi: 表示π的值，如果不传，则默认3.14

function area_of_circle(r, pi) {

}
*/
console.log(`======= 第6题 =======`);

function area_of_circle(r, pi) {
    if (pi === undefined) {
        pi = 3.14
    }
    console.log(pi * r * r);
}
area_of_circle(4, 3)
area_of_circle(4)//不传

/*
7、题目描述
用jQuery编程实现获取选中复选框值的函数abc
HTML结构如下：
<div>
    <input type="checkbox" name="aa" value="0" />0
    <input type="checkbox" name=" aa " value="1" />1
    <input type="checkbox" name=" aa " value="2" />2
    <input type="checkbox" name=" aa " value="3" />3
    <input type="button" onclick="abc()" value="提 交" />
    <div id="allselect"></div>
</div>

*/
console.log(`======= 第7题 =======`);
var div = $('#allselect')
div.c
function abc() {
    console.log('我是函数abc的哈喽');
}
div.click(abc)

/*
8、题目描述
实现sel函数显示当前选项的文本和值
<div>
    <form name="a">
        <select name="a" size="1" onchange="_sel(this)">
        <option value="a">1</option>
        <option value="b">2</option>
        <option value="c">3</option>
        </select>
    </form>
</div>

function getOptionVal(){

}
*/

console.log(`======= 第8题 =======`);
var select = $('select[name=a]')
console.log(select);

function sel() {
    var aa = $('option[value=a]')

    console.log(aa[0].value);
    console.log(aa[0].text);
    
    var a1 = $('option[value=b]')
    if (a1.value === 2) {
        console.log(a1[1].value);
        console.log(a1[1].text);

    }

     
    var a2 = $('option[value=c]')
    if (a2.value === 3) {

        console.log(a2[2].value);
        console.log(a2[2].text);
    }

}
select.click(sel)


/*
9、题目描述
要求用jQuery完成:  点击id为btn的按钮

a.判断id为username的输入是否为空，如果为空，则弹出“用户名不能为空”的提示。

b.判断id为password的输入字符串个数是否小于6位，如果小于6位，则弹出“你输入的密码长度不可以少于6位”的提示

HTML结构如下：
<div>
    用户名：<input type="text" id="username"/><br/>
    密  码：<input type="password" id="password"/><br/>
　　 确认密码：<input type="password" id="password1"/><br/>
    <button id="btnSubmit" type="button">提交</button><br/>
</div>
*/
console.log(`======= 第9题 =======`);
var id = $('#btnSubmit')

function btn() {
    var name = $('#username')
    var password = $('#password')

    if (password[0].value === undefined) {
        alert('你输入的密码长度不可以少于6位');
    }
    if (name[0].value === undefined) {
        alert('用户名不能为空');
    }

    name.text = name[0].value
    console.log(name.text);
    password.text = password[0].value
    console.log(password.text);

}
id.click(btn)

/*
10、题目描述

在下面的HTML文档中，编写函数test() ,实现如下功能：

（1）当多行文本框中的字符数超过20个，只截取20个字符

（2）在id为number的td中显示文本框的字符个数

HTML结构如下：
<table>
    <tr>
        <td>留言</td>
        <td id="number"> 0 </td>
    </tr>
    <tr>
        <td colspan=2>
            <textarea id="feedBack" onkeyup="test()" rows=6></textarea>
        </td>
    </tr>
</table>


function test(){
    
}
*/
console.log(`======= 第10题 =======`);
//(1)
var arr = $('#feedBack')
function test() {

    if (arr.length > 20) {
        return b.map(function () {
            return b.slice(0, 20)
        })
    }
    var number = $('#number')
    number[0].value === arr.length

    console.log(nmber[0].value);
}
arr.click(test)


