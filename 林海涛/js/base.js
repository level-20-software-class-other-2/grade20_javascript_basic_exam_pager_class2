// 题目如下，请直接利用写好的方法，并且直接写在题目下方，要可以运行和输出，如第1题所示

/*
1、题目描述：
找出元素 item 在给定数组 arr 中的位置
输出描述:
如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
示例1
输入
[ 1, 2, 3, 4 ], 3
输出
2

function indexOf(arr, item) {

}
*/
console.log(`======= 第1题 =======`);
function indexOf(arr, item) {
    return arr.indexOf(item)
}
var res = [ 1, 2, 3, 4 ]
console.log(indexOf(res,3));
/*
2、题目描述：
计算给定数组 arr 中所有元素的总和
输入描述:
数组中的元素均为 Number 类型
示例1
输入

[ 1, 2, 3, 4 ]
输出

10

function sum(arr) {

}
*/
console.log(`======= 第2题 =======`);
function sum(arr) {
    var res = arr.reduce(function(x,y){
        return x+y
    })
    return res
}
let res2 = sum([1,2,3,4])
console.log(res2);

/*
3、题目描述
在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4],  10
输出

[1, 2, 3, 4, 10]

function append(arr, item) {
    
}
*/
console.log(`======= 第3题 =======`);
function append(arr, item) {
    arr.push(item)
    var new_arr = arr
    return new_arr
}
var res3 = append([1, 2, 3, 4],10)
console.log(res3);

/*
4、题目描述
合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], ['a', 'b', 'c', 1]
输出

[1, 2, 3, 4, 'a', 'b', 'c', 1]

function concat(arr1, arr2) {

}
*/
console.log(`======= 第4题 =======`);
function con_arr(arr1,arr2) {
    var arr = [];
    return arr.concat(arr1,arr2)
}
var arr1 = [1,2,3,4]
var arr2 = ['a', 'b', 'c', 1]
var res4 = con_arr(arr1,arr2)
console.log(res4);

/*
5、题目描述
为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[1, 4, 9, 16]

function square(arr) {

}
*/
console.log(`======= 第5题 =======`);
function square(x) {
    return x * x
}
var arr = [1, 2, 3, 4]
var new_arr = arr.map(square)
console.log(new_arr);


/*
6、题目描述
定义一个计算圆面积的函数area_of_circle()，它有两个参数：

r: 表示圆的半径；
pi: 表示π的值，如果不传，则默认3.14

function area_of_circle(r, pi) {
var r 
    var pi = 3.14 || pi
    return r*r*pi
}
*/
console.log(`======= 第6题 =======`);
function area_of_circle(r, pi) {
    var r 
    var pi = 3.14 || pi
    return r*r*pi
}
var res6 = area_of_circle(3,3.14)
console.log(res6);


/*
7、题目描述
用jQuery编程实现获取选中复选框值的函数abc
HTML结构如下：
<div>
    <input type="checkbox" name="aa" value="0" />0
    <input type="checkbox" name=" aa " value="1" />1
    <input type="checkbox" name=" aa " value="2" />2
    <input type="checkbox" name=" aa " value="3" />3
    <input type="button" onclick="abc()" value="提 交" />
    <div id="allselect"></div>
</div>

*/
console.log(`======= 第7题 =======`);
var bu = $(['onclick=abc()'])
console.log(bu);

/*
8、题目描述
实现sel函数显示当前选项的文本和值
<div>
    <form name="a">
        <select name="a" size="1" onchange="_sel(this)">
        <option value="a">1</option>
        <option value="b">2</option>
        <option value="c">3</option>
        </select>
    </form>
</div>

function getOptionVal(){

}
*/
console.log(`======= 第8题 =======`);

function getOptionVal(){
    var areid=$('value').val()
    console.log(areid);
}

/*
9、题目描述
要求用jQuery完成:  点击id为btn的按钮

a.判断id为username的输入是否为空，如果为空，则弹出“用户名不能为空”的提示。

b.判断id为password的输入字符串个数是否小于6位，如果小于6位，则弹出“你输入的密码长度不可以少于6位”的提示

HTML结构如下：
<div>
    用户名：<input type="text" id="username"/><br/>
    密  码：<input type="password" id="password"/><br/>
　　 确认密码：<input type="password" id="password1"/><br/>
    <button id="btnSubmit" type="button">提交</button><br/>
</div>
*/
console.log(`======= 第9题 =======`);
var username = $('#username'),
    password = $('#password'),
    password1 = $('#password1'),
    btn = $('#btnSubmit')

$(function(){
    btn.on('click',function(){
        if(username.val() === ''){
            alert('用户名输入不能为空')
        }
        if(password.val().length<6){
            alert('你输入的密码不可以少于6位')
        }
    })
})


/*
10、题目描述

在下面的HTML文档中，编写函数test() ,实现如下功能：

（1）当多行文本框中的字符数超过20个，只截取20个字符

（2）在id为number的td中显示文本框的字符个数

HTML结构如下：
<table>
    <tr>
        <td>留言</td>
        <td id="number"> 0 </td>
    </tr>
    <tr>
        <td colspan=2>
            <textarea id="feedBack" onkeyup="test()" rows=6></textarea>
        </td>
    </tr>
</table>


function test(){
    
}
*/
console.log(`======= 第10题 =======`);
var fb = $('#feedBack')
console.log(fb);
function test(){
    if(fb.val()>20){
        
    }
}