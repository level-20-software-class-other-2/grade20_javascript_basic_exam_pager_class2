// 题目如下，请直接利用写好的方法，并且直接写在题目下方，要可以运行和输出，如第1题所示

/*
1、题目描述：
找出元素 item 在给定数组 arr 中的位置
输出描述:
如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
示例1
输入
[ 1, 2, 3, 4 ], 3
输出
2

function indexOf(arr, item) {

}
*/
console.log(`======= 第1题 =======`);
function indexOf(arr, item) {
    return arr.indexOf(item)
}
console.log(indexOf([ 1, 2, 3, 4 ], 3));
/*
2、题目描述：
计算给定数组 arr 中所有元素的总和
输入描述:
数组中的元素均为 Number 类型
示例1
输入

[ 1, 2, 3, 4 ]
输出

10

function sum(arr) {

}
*/
console.log(`======= 第2题 =======`);
function sum(arr) {
    let s=0
    for (let i = 0; i < arr.length; i++) {
        s+=arr[i]
        
    }
    return s
}
console.log(sum([ 1, 2, 3, 4 ]));
/*
3、题目描述
在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4],  10
输出

[1, 2, 3, 4, 10]

function append(arr, item) {
    
}
*/
console.log(`======= 第3题 =======`);
function append(arr, item) {
    let arr2=[...arr]
    arr2.push(item)
    return arr2
}
console.log(append([1, 2, 3, 4],  10));
/*
4、题目描述
合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], ['a', 'b', 'c', 1]
输出

[1, 2, 3, 4, 'a', 'b', 'c', 1]

function concat(arr1, arr2) {

}
*/
console.log(`======= 第4题 =======`);
function concat(arr1, arr2) {
    let arr3=arr1.concat(arr2)
    return arr3
}
console.log(concat([1, 2, 3, 4], ['a', 'b', 'c', 1]));
/*
5、题目描述
为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[1, 4, 9, 16]

function square(arr) {

}
*/
console.log(`======= 第5题 =======`);
function square(arr) {
    return arr.map((x)=>{
        return x*x
    })
}
console.log(square([1, 2, 3, 4]));
/*
6、题目描述
定义一个计算圆面积的函数area_of_circle()，它有两个参数：

r: 表示圆的半径；
pi: 表示π的值，如果不传，则默认3.14

function area_of_circle(r, pi) {

}
*/
console.log(`======= 第6题 =======`);
function area_of_circle(r, pi=3.14) {
    return r*r*pi
}
console.log(area_of_circle(1));
/*
7、题目描述
用jQuery编程实现获取选中复选框值的函数abc
HTML结构如下：
<div>
    <input type="checkbox" name="aa" value="0" />0
    <input type="checkbox" name=" aa " value="1" />1
    <input type="checkbox" name=" aa " value="2" />2
    <input type="checkbox" name=" aa " value="3" />3
    <input type="button" onclick="abc()" value="提 交" />
    <div id="allselect"></div>
</div>

*/
console.log(`======= 第7题 =======`);
let t=$('input[type=checkbox]')
let bt=$('[type=button]')
let bt2=bt.get(0)
function abc(){
    let x=''
    for (let i = 0; i < t.length; i++) {
        if (t[i].checked) {
            x+=t[i].value
        }
    }
    alert(x)
}
console.log(bt2);
/*
8、题目描述
实现sel函数显示当前选项的文本和值
<div>
    <form name="a">
        <select name="a" size="1" onchange="_sel(this)">
        <option value="a">1</option>
        <option value="b">2</option>
        <option value="c">3</option>
        </select>
    </form>
</div>

function getOptionVal(){

}
*/
console.log(`======= 第8题 =======`);

// function getOptionVal(){
   
// }
let cw= document.querySelector('[name=a]')
let c=document.querySelectorAll('div option')
function _sel(){    
    for (let l = 0; l < c.length; l++) {
        console.log(c[l].innerHTML,c[l].value);
        
    }
    
}

cw.onchange=_sel()

/*
9、题目描述
要求用jQuery完成:  点击id为btn的按钮

a.判断id为username的输入是否为空，如果为空，则弹出“用户名不能为空”的提示。

b.判断id为password的输入字符串个数是否小于6位，如果小于6位，则弹出“你输入的密码长度不可以少于6位”的提示

HTML结构如下：
<div>
    用户名：<input type="text" id="username"/><br/>
    密  码：<input type="password" id="password"/><br/>
　　 确认密码：<input type="password" id="password1"/><br/>
    <button id="btnSubmit" type="button">提交</button><br/>
</div>
*/
console.log(`======= 第9题 =======`);
$('div #btnSubmit')
$('#username')
$("#password")
$('#password1')
let tj=document.querySelector('div #btnSubmit')
let yhm=document.querySelector('#username')
let mm=document.querySelector("#password")
let qrmm=document.querySelector('#password1')

tj.onclick=function(){
    
    if (yhm.value=='') {
        alert('用户名不可为空')
    }
    if (mm.value.length<6) {
        alert('你输入的密码长度不可以少于6位')
    }
    if (mm.value!=qrmm.value) {
        alert('两次密码不一致')
    }
    
}
console.log(yhm);
console.log(tj);
console.log(mm);
/*
10、题目描述

在下面的HTML文档中，编写函数test() ,实现如下功能：

（1）当多行文本框中的字符数超过20个，只截取20个字符

（2）在id为number的td中显示文本框的字符个数

HTML结构如下：
<table>
    <tr>
        <td>留言</td>
        <td id="number"> 0 </td>
    </tr>
    <tr>
        <td colspan=2>
            <textarea id="feedBack" onkeyup="test()" rows=6></textarea>
        </td>
    </tr>
</table>


function test(){
    
}
*/
console.log(`======= 第10题 =======`);

let number=document.querySelector('#number')
let feedBack=document.querySelector('#feedBack')

let i=0

function test(){
    
    
    i++
    if (i>20) {
        number.innerHTML=20
    }else{
        number.innerHTML=i
    }
}








