// 题目如下，请直接利用写好的方法，并且直接写在题目下方，要可以运行和输出，如第1题所示

/*
1、题目描述：
找出元素 item 在给定数组 arr 中的位置
输出描述:
如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
示例1
输入
[ 1, 2, 3, 4 ], 3
输出
2

function indexOf(arr, item) {

}
*/
// console.log(`======= 第1题 =======`);
function indexOf(arr, item) {
    return arr.indexOf(item)
}
let result1 =  indexOf([1,2,3,4],3) // 2
// console.log(result1);

/*
2、题目描述：
计算给定数组 arr 中所有元素的总和
输入描述:
数组中的元素均为 Number 类型
示例1
输入

[ 1, 2, 3, 4 ]
输出

10
*/
// console.log(`======= 第2题 =======`);
function sum(arr) {
   return arr.reduce(function(a,b){
        return a+b;
    })
}
let result2 = sum([ 1, 2, 3, 4 ])
// console.log(result2);

/*
3、题目描述：
返回给定数组arr中偶数
输入描述:
数组中的元素均为 Number 类型
示例1
输入

[ 1, 2, 3, 4 ]
输出

[2,4]
*/
// console.log(`======= 第3题 =======`);
function filter(arr) {
    return arr.filter(function(e){
       return e%2===0
    })
}
let result3 = filter([ 1, 2, 3, 4 ])
// console.log(result3);


/*
4、题目描述
在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4],  10
输出

[1, 2, 3, 4, 10]
*/
// console.log(`======= 第4题 =======`);
function append(arr, item) {
    let newarr1 = arr.slice();
    newarr1.push(item)
    return newarr1;
}
let result4 = append([1, 2, 3, 4],  10)
// console.log(result4);


/*
5、题目描述
合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], ['a', 'b', 'c', 1]
输出

[1, 2, 3, 4, 'a', 'b', 'c', 1]

*/
// console.log(`======= 第5题 =======`);
function concat(arr1, arr2) {
        //分别复制两份arr数组
       let newarr2 = arr1.slice();
       let newarr3 = arr2.slice();
       return newarr2.concat(newarr3);
   
}
let result5 =concat([1, 2, 3, 4], ['a', 'b', 'c', 1]);
// console.log(result5);



/*
6、题目描述
为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[1, 4, 9, 16]
*/
// console.log(`======= 第6题 =======`);
function square(arr) {
   return arr.map(a=>{
        return a*a;
    })
}   
let result6 = square([1, 2, 3, 4])
// console.log(result6);


/*
7、题目描述
为给定数组arr去重，不要直接修改数组arr，结果返回新的数组
示例1
输入

[1, 1, 3, 4, 1]
输出

[1, 3, 4]
*/
// console.log(`======= 第7题 =======`);
function distinct(arr) {
    let newarr4 = arr.slice();
    return newarr4.filter((e,i,s)=>{
        return s.lastIndexOf(e)===i
    }).sort((a,b)=>(a-b))
}
let result7 = distinct([1, 1, 3, 4, 1])
// console.log(result7);


/*
8、题目描述
定义一个计算圆面积的函数area_of_circle()，它有两个参数：

r: 表示圆的半径；
pi: 表示π的值，如果不传，则默认3.14
*/
// console.log(`======= 第8题 =======`);
function area_of_circle(r, pi) {
    //若pi不存在则取3.14
    pi = pi||3.14;
    return r*r*pi;
}
let result8 = area_of_circle(5,5)
// console.log(result8);


/*
9、题目描述
用jQuery编程实现获取选中复选框值的函数abc
HTML结构如下：
<div>
    <input type="checkbox" name="aa" value="0" />0
    <input type="checkbox" name=" aa " value="1" />1
    <input type="checkbox" name=" aa " value="2" />2
    <input type="checkbox" name=" aa " value="3" />3
    <input type="button" onclick="abc()" value="提 交" />
    <div id="allselect"></div>
</div>
*/

console.log(`======= 第9题 =======`);
/*
function abc(){
    //获取所有的属性为checkbox 的 input jq对象
    let inval = $('[type = checkbox]')
     inval.map((i,e)=>{
        e就是每个inputjq对象
        if(e.checked){
            console.log($(e).val());
            return $(e).val();
        }
    })
}
*/

/*
10、题目描述
实现getOptionVal函数显示当前选项的文本和值
<div>
    <form name="a">
        <select name="a" size="1" onchange="_sel(this)">
        <option value="a">1</option>
        <option value="b">2</option>
        <option value="c">3</option>
        </select>
    </form>
</div>
*/
 console.log(`======= 第10题 =======`);


/*
function getOptionVal(){
    // $('select').change(_sel)
    //调用 select 的 _sel(this) 函数
    _sel();
}
function _sel(){
    let options = $('option');
    options.map(function(i,e){
        if(e.selected){
            console.log('当前选项的文本和值为：'+ e.value,e.innerText);
        }
    })
}
getOptionVal()

*/

/*
function getOptionVal(){
    $('select').change(_sel)
}
function _sel(){
    let options = $('option');
    options.map(function(i,e){
        if(e.selected){
            console.log(e.value,e.innerText);
        }
    })
}

getOptionVal()
*/


/*
11、题目描述
要求用jQuery完成:  点击id为btn的按钮(要求使用事件绑定)

a.判断id为username的输入是否为空，如果为空，则弹出“用户名不能为空”的提示。

b.判断id为password的输入字符串个数是否小于6位，如果小于6位，则弹出“你输入的密码长度不可以少于6位”的提示

HTML结构如下：
<div>
    用户名：<input type="text" id="username"/><br/>
    密  码：<input type="password" id="password"/><br/>
　　 确认密码：<input type="password" id="password1"/><br/>
    <button id="btnSubmit" type="button">提交</button><br/>
</div>
*/
console.log(`======= 第11题 =======`);

/*
function btnClick(){
    console.log($('#btnSubmit'));
    //获取button按钮
    let btn = $('#btnSubmit');
    //获取用户名输入框
    let name = $('#username')
    //获取password输入框
    let paw = $('#password')
    //绑定button按钮的点击事件
    btn.click(function(){
        //判断用户名是否为空
         if(name.val().length===0){     
             alert('用户名不能为空');
         }
         //判断输入的密码长度
        if(paw.val().length<6){      
            alert('你输入的密码长度不可以少于6位');
        }
    })
}
btnClick()
*/


/*
12、题目描述

在下面的HTML文档中，编写函数test() ,实现如下功能：

（1）当多行文本框中的字符数超过20个，只截取20个字符

（2）在id为number的td中显示文本框的字符个数

HTML结构如下：
<table>
    <tr>
        <td>留言</td>
        <td id="number"> 0 </td>
    </tr>
    <tr>
        <td colspan=2>
            <textarea id="feedBack" onkeyup="test()" rows=6></textarea>
        </td>
    </tr>
</table>

// */
console.log(`======= 第12题 =======`);
/*
function test(){
        //获取多行文本框
        let textarea = $('#feedBack');

        if(textarea.val().length>=5){
            textarea[0].innerText=textarea.val().substring(0,20)
              //如果个数大于20则显示20个
            console.log(textarea.val());
        }
        //获取留言
        let number = $('#number');
        //得到文本框留言字数
        number[0].innerText =  textarea.val().length
}

//test();
*/