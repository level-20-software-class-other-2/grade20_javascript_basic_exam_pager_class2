// 题目如下，请直接利用写好的方法，并且直接写在题目下方，要可以运行和输出，如第1题所示

/*
1、题目描述：
找出元素 item 在给定数组 arr 中的位置
输出描述:
如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
示例1
输入
[ 1, 2, 3, 4 ], 3
输出
2

function indexOf(arr, item) {

}
*/
console.log(`======= 第1题 =======`);
function indexOf(arr, item) {
    return arr.indexOf(item);
}
var arr2 =  indexOf([1,2,3,4],3); // 2
console.log(arr2);
/*
2、题目描述：
计算给定数组 arr 中所有元素的总和
输入描述:
数组中的元素均为 Number 类型
示例1
输入

[ 1, 2, 3, 4 ]
输出

10

function sum(arr) {

}
*/
console.log(`======= 第2题 =======`);
function sum(arr) {
    var arr1 = arr.reduce(function (x, y) {
        return x + y;
    });
    return arr1;
}
var arr2 = sum([ 1, 2, 3, 4 ]);
console.log(arr2);
/*
3、题目描述：
返回给定数组arr中偶数
输入描述:
数组中的元素均为 Number 类型
示例1
输入

[ 1, 2, 3, 4 ]
输出

[2,4]

function filter(arr) {

}
*/
console.log(`======= 第3题 =======`);
function filter(arr) {
    var arr1 = [];
    arr.forEach(function (n) {
        if (n % 2 == 0) {
            arr1.push(n);
        }
    });
    return arr1;
}
var arr2 = filter([ 1, 2, 3, 4 ]);
console.log(arr2);

/*
4、题目描述
在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4],  10
输出

[1, 2, 3, 4, 10]

function append(arr, item) {
    
}
*/
console.log(`======= 第4题 =======`);
function append(arr, item) {
    var arr1 = arr;
    arr.push(item);
    return arr1;
}
var arr2 = append([1, 2, 3, 4],  10);
console.log(arr2);
/*
5、题目描述
合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], ['a', 'b', 'c', 1]
输出

[1, 2, 3, 4, 'a', 'b', 'c', 1]

function concat(arr1, arr2) {

}
*/
console.log(`======= 第5题 =======`);
function concat(arr1, arr2) {
    var arr3 = arr1.concat(arr2);
    return arr3;
}
var arr = concat([1, 2, 3, 4], ['a', 'b', 'c', 1]);
console.log(arr);
/*
6、题目描述
为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[1, 4, 9, 16]

function square(arr) {

}
*/
console.log(`======= 第6题 =======`);
function square(arr) {
    var arr1 =[];
    arr.forEach(function (x) {
        arr1.push(x * x);
    });
    return arr1
}
var arr2 = square([1, 2, 3, 4]);
console.log(arr2);
/*
7、题目描述
为给定数组arr去重，不要直接修改数组arr，结果返回新的数组
示例1
输入

[1, 1, 3, 4, 1]
输出

[1, 3, 4]

function distinct(arr) {

}
*/
console.log(`======= 第7题 =======`);
function distinct(arr) {
    var arr1 = new Set(arr);;
    return arr1;
}
var arr2 = distinct([1, 1, 3, 4, 1]);
console.log(arr2);
/*
8、题目描述
定义一个计算圆面积的函数area_of_circle()，它有两个参数：

r: 表示圆的半径；
pi: 表示π的值，如果不传，则默认3.14

function area_of_circle(r, pi) {

}
*/
console.log(`======= 第8题 =======`);
function area_of_circle(r, pi) {
    var pai = pi || 3.14;
    return r * r * pai
}
var arr2 = area_of_circle(3);
console.log(arr2);

/*
9、题目描述
用jQuery编程实现获取选中复选框值的函数abc
HTML结构如下：
<div>
    <input type="checkbox" name="aa" value="0" />0
    <input type="checkbox" name=" aa " value="1" />1
    <input type="checkbox" name=" aa " value="2" />2
    <input type="checkbox" name=" aa " value="3" />3
    <input type="button" onclick="abc()" value="提 交" />
    <div id="allselect"></div>
</div>

function abc(){

}
*/
console.log(`======= 第9题 =======`);
function abc(){
    console.log($('[name=aa]'));
    console.log($('.value'));
}
/*
10、题目描述
实现getOptionVal函数显示当前选项的文本和值
<div>
    <form name="a">
        <select name="a" size="1" onchange="_sel(this)">
        <option value="a">1</option>
        <option value="b">2</option>
        <option value="c">3</option>
        </select>
    </form>
</div>

function getOptionVal(){

}
*/
console.log(`======= 第10题 =======`);
function getOptionVal(){
    var a = document.getOptionVal('a');
    console.log(a);
}
/*
11、题目描述
要求用jQuery完成:  点击id为btn的按钮(要求使用事件绑定)

a.判断id为username的输入是否为空，如果为空，则弹出“用户名不能为空”的提示。

b.判断id为password的输入字符串个数是否小于6位，如果小于6位，则弹出“你输入的密码长度不可以少于6位”的提示

HTML结构如下：
<div>
    用户名：<input type="text" id="username"/><br/>
    密  码：<input type="password" id="password"/><br/>
　　 确认密码：<input type="password" id="password1"/><br/>
    <button id="btnSubmit" type="button">提交</button><br/>
</div>

function btnClick(){

}
*/
var username = $('#username');
var password = $('#password');
var password1 = $('#password1');
function btnClick(){
    if (username !== null) {
        if (password >= 6 && password !== null) {
            if (password === password1) {
            }
        }
    } else {
        console.log("用户名不能为空");
    }
}
/*
12、题目描述

在下面的HTML文档中，编写函数test() ,实现如下功能：

（1）当多行文本框中的字符数超过20个，只截取20个字符

（2）在id为number的td中显示文本框的字符个数

HTML结构如下：
<table>
    <tr>
        <td>留言</td>
        <td id="number"> 0 </td>
    </tr>
    <tr>
        <td colspan=2>
            <textarea id="feedBack" onkeyup="test()" rows=6></textarea>
        </td>
    </tr>
</table>


function test(){
    
}
*/
console.log(`======= 第12题 =======`);
//当多行文本框中的字符数超过20个，只截取20个字符
    var feedBackt = document.getElementById('feedBack');
    var number = document.getElementById('number');
function test(){
    if (text >= 20) {
        text.submit(0,20);
    }
}
